import "./App.css";

import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

import { Home } from "./pages/Home/Home";
import NewRecipe from "./pages/Recipe/NewRecipe";

const App = () => {
  return (
    <div className="container">
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/recipe/new" component={NewRecipe} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
