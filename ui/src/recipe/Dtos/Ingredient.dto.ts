export default class IngredientDto {
  constructor(public name = "", public measurement = "") {}
}
