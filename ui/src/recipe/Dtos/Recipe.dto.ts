import IngredientDto from "./Ingredient.dto";

export default class RecipeDto {
  constructor(
    public name: string = "",
    public cookingMethod: string = "",
    public ingredients: IngredientDto[] = [],
    public id?: number
  ) {}
}
