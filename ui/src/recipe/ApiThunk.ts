import { createAsyncThunk } from "@reduxjs/toolkit";
import { Axios } from "axios";
import RecipeDto from "./Dtos/Recipe.dto";
import { add, init, remove, search } from "./RecipeSlice";

const axios = new Axios({
  baseURL: "http://localhost:3080",
  headers: {
    "Content-Type": "application/json",
  },
});

export const removeRecipe = createAsyncThunk(
  "recipes/deleteByRecipe",
  async (recipe: RecipeDto, thunkApi): Promise<void> => {
    await axios.delete(`recipes/${recipe.id}`);
    thunkApi.dispatch(remove(recipe));
  }
);

export const fetchRecipe = createAsyncThunk(
  "recipes/recipesLoaded",
  async (keywords: string, thunkApi) => {
    console.log(keywords);

    const params =
      keywords === ""
        ? {}
        : {
            params: {
              search: keywords,
            },
          };

    const response = await axios.get<RecipeDto[]>("recipes", params);

    if (typeof response.data === "string") {
      const recipeObj = JSON.parse(response.data);
      const recipes = recipeObj.map((v: RecipeDto) => {
        return { ...new RecipeDto(), ...v };
      });
      thunkApi.dispatch(init(recipes));
    } else {
      const recipes = response.data.map((v: RecipeDto) => {
        return { ...new RecipeDto(), ...v };
      });
      thunkApi.dispatch(init(recipes));
    }
  }
);

export const searchRecipe = createAsyncThunk(
  "recipes/researchRecipe",
  async (keyword: string, thunkApi): Promise<void> => {
    if (keyword.length > 2) {
      thunkApi.dispatch(fetchRecipe(keyword));
    }
    thunkApi.dispatch(search(keyword));
  }
);

export const createRecipe = createAsyncThunk(
  "recipes/create",
  async (recipe: RecipeDto, thunkApi) => {
    const response = await axios.post(`recipes/`, JSON.stringify(recipe));

    if (typeof response.data === "string") {
      const inserted = JSON.parse(response.data);
      thunkApi.dispatch(add({ ...new RecipeDto(), ...inserted }));
    } else {
      thunkApi.dispatch(add({ ...new RecipeDto(), ...response.data }));
    }
  }
);
