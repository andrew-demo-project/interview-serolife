import Recipe from "./Recipe";
import SearchBar from "./SearchBar";
import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import RecipeDto from '../Dtos/Recipe.dto';
import { Link } from "react-router-dom";

export default function RecipeContainer() {

    const recipeData = useSelector((state: RootState) => state.recipe.recipes) as RecipeDto[];

    const recipes = recipeData.map((recipe, index) => {
        return (
            // todo change key to recipe id
            <Recipe recipeDto={recipe} key={recipe.name + index} />
        )
    });

    return (
        <div className="recipe-container">
            <h1>Recipe</h1>
            <Link to="/recipe/new">Create new Recipe</Link>
            <SearchBar />
            <div className="row g-2">
                {recipes}
            </div>
        </div>
    )
}