import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store';
import { searchRecipe } from '../ApiThunk';

export default function SearchBar() {
    const keyword = useSelector((state: RootState) => state.recipe.keyword);
    const dispatch = useDispatch();
    return (
        <form className="form-row-container">
            <label htmlFor="recipe-search" className="form-label">Find your recipes</label>
            <input
                type="text"
                className="form-control"
                name="recipe-search"
                id="recipe-search"
                value={keyword}
                onChange={(e) => dispatch(searchRecipe(e.target.value))}
            />
            <span className="form-text">Input your recipe name or ingredients</span>
        </form>
    )
}