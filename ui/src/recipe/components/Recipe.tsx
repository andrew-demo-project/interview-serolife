import RecipeDto from '../Dtos/Recipe.dto';
import { useDispatch } from 'react-redux';
import { removeRecipe } from '../ApiThunk';

export default function Recipe(prop: { recipeDto: RecipeDto }) {
    const dispatch = useDispatch();

    const ingredients = prop.recipeDto.ingredients.map((ingredient, index) => {
        return (
            // todo change key to ingredient id
            <li
                key={ingredient.name + index}
                className="list-group-item"
            >
                <div className="row">
                    <div className="col-8">{ingredient.name}</div>
                    <div className="col-4">{ingredient.measurement}</div>
                </div>
            </li>
        )
    });

    return (
        <div className="col-4">
            <div className="card">
                <div className="card-header">
                    {prop.recipeDto.name}
                    <button
                        type="button"
                        className="btn-close"
                        aria-label="Close"
                        onClick={() => dispatch(removeRecipe(prop.recipeDto))}
                    ></button>
                </div>
                <div className="card-body">
                    <p className="card-body">{prop.recipeDto.cookingMethod}</p>
                    <ul className="list-group list-group-flush">{ingredients}</ul>
                </div>
            </div>
        </div>
    )
}
