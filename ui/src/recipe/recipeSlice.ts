import { createSlice } from "@reduxjs/toolkit";
import RecipeDto from "./Dtos/Recipe.dto";

const emptyRecipe = {
  name: "",
  cookingMethod: "",
  ingredients: [],
} as RecipeDto;

export const RecipeSlice = createSlice({
  name: "recipe",
  initialState: {
    keyword: "",
    recipes: [] as RecipeDto[],
    newRecipe: emptyRecipe,
    createCompleted: false,
  },
  reducers: {
    init: (state, action) => {
      state.recipes = action.payload;
    },
    updateNewRecipes: (state, action) => {
      state.newRecipe = { ...state.newRecipe, ...action.payload };
    },
    add: (state, action) => {
      state.recipes = [action.payload].concat(state.recipes) as RecipeDto[];
      state.newRecipe = emptyRecipe;
      state.createCompleted = true;
    },
    remove: (state, action) => {
      const idToRemove = action.payload.id;
      state.recipes = state.recipes.filter(
        (recipe: RecipeDto) => recipe.id !== idToRemove
      );
    },
    clearSubmitCompleteState: (state) => {
      state.createCompleted = false;
    },
    search: (state, action) => {
      state.keyword = action.payload;
    },
  },
});

export const {
  init,
  add,
  search,
  remove,
  updateNewRecipes,
  clearSubmitCompleteState,
} = RecipeSlice.actions;

export default RecipeSlice.reducer;
