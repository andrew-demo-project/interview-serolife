import { Link, Redirect } from "react-router-dom";
import RecipeDto from '../../recipe/Dtos/Recipe.dto';
import IngredientDto from '../../recipe/Dtos/Ingredient.dto';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store';
import { clearSubmitCompleteState, updateNewRecipes } from "../../recipe/RecipeSlice";
import { createRecipe } from '../../recipe/ApiThunk';
import { useEffect } from 'react';

export default function NewRecipe() {
    const newRecipe = useSelector((state: RootState) => state.recipe.newRecipe) as RecipeDto;
    const shouldRedirect = useSelector((state: RootState) => state.recipe.createCompleted);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(clearSubmitCompleteState())
    });

    const updateIngredient = function (dirtyProps: {}, index: number) {
        const ingredients = newRecipe.ingredients.slice();

        if (ingredients[index] == null) {
            ingredients.push(new IngredientDto());
        }

        ingredients[index] = { ...ingredients[index], ...dirtyProps }

        dispatch(updateNewRecipes({ ingredients }))
    }

    const ingredientsHasEmptyField = (): boolean => {
        const count = newRecipe.ingredients.filter(
            (i) => i.name === '' || i.measurement === ''
        ).length;
        return count !== 0
    }

    const renderIngredient = () => {
        const ingredients = newRecipe.ingredients;
        const ingredientCount = ingredients.length + (ingredientsHasEmptyField() ? 0 : 1);

        let ingredientsObj = [];
        for (let i = 0; i < ingredientCount; i++) {
            ingredientsObj.push(
                (
                    <div className="mb-3 row" key={i}>
                        <div className="col-8">
                            <input
                                id={`ingredient-name-${i}`}
                                type="text form-control"
                                name="ingredient"
                                className="form-control"
                                placeholder="ingredient"
                                onChange={(e) => updateIngredient({ name: e.target.value }, i)}
                            />
                        </div>
                        <div className="col-4">
                            <input
                                id={`ingredient-measurement-${i}`}
                                type="text form-control"
                                name="measurement"
                                className="form-control"
                                placeholder="measurement"
                                onChange={(e) => updateIngredient({ measurement: e.target.value }, i)}
                            />
                        </div>
                    </div>
                )
            );
        }
        return ingredientsObj;
    }

    const readyToSubmit = !ingredientsHasEmptyField() && (newRecipe.name !== '');

    if (shouldRedirect) {
        return <Redirect to="/" />;
    }

    return (
        <div>
            <h1> New recipe </h1>
            <form className="row g-3 mt-0">
                <div className="mb-3 row">
                    <label htmlFor="name" className="col-3 col-form-label">Recipe name</label>
                    <div className="col-9">
                        <input
                            type="text"
                            className="form-control"
                            id="name"
                            name="name"
                            value={newRecipe.name}
                            onChange={e => dispatch(updateNewRecipes({ name: e.target.value }))}
                        />
                    </div>
                </div>
                <div className="mb-3 row">
                    <label htmlFor="cooking-method" className="col-3 col-form-label">Cooking method</label>
                    <div className="col-9">
                        <textarea
                            name="cooking-method"
                            id="cooking-method"
                            className="form-control"
                            value={newRecipe.cookingMethod}
                            onChange={e => dispatch(updateNewRecipes({ cookingMethod: e.target.value }))}
                        ></textarea>
                    </div>
                </div>
                <h3>Ingredients</h3>

                {renderIngredient()}

                <div className="row justify-content-around">
                    <Link to="/" className="col-auto" >Back to home</Link>
                    <button
                        type="button"
                        className="btn btn-primary col-auto"
                        disabled={!readyToSubmit}
                        onClick={() => dispatch(createRecipe(newRecipe))}
                    >Submit</button>
                </div>
            </form>
        </div >
    )
}