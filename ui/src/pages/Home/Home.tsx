import "./home.css";
import RecipeContainer from '../../recipe/components/RecipeContainer';

export const Home = () => {
  return (
    <div>
      <RecipeContainer />
    </div>
  );
};
