describe("Recipe tests", () => {
  it(`Given I have a new recipe
      When I add the new recipe name
      And ingredients
      And measurements
      And cooking method
      Then the new recipe is saved for later`, () => {
    cy.visit("http://localhost:3000/recipe/new");
    cy.get("#name").type("new recipe").should("have.value", "new recipe");
    cy.get("#cooking-method")
      .type("add some magic")
      .should("have.value", "add some magic");
    cy.get("#ingredient-name-0").type("apple").should("have.value", "apple");
    cy.get("#ingredient-measurement-0").type("1g").should("have.value", "1g");
    cy.get(".btn-primary").click();
    cy.contains("new recipe");
  });

  it(`Given I want to look for a recipe
      When I search by the name of the recipe
      Then I find the recipe
      And I can see the ingredients
      And I can see the cooking methods`, () => {
    cy.visit("http://localhost:3000/");
    cy.get("#recipe-search").type("new recipe");
    cy.contains("new recipe");
    cy.contains("add some magic");
    cy.contains("apple");
    cy.contains("1g");
  });

  it(`Given I want to look for a recipe by ingredients
      When I search by the ingredient of the recipe
      Then I find the recipe
      And I can see the ingredients
      And I can see the cooking methods`, () => {
    cy.visit("http://localhost:3000/");
    cy.get("#recipe-search").type("apple");
    cy.contains("new recipe");
    cy.contains("add some magic");
    cy.contains("apple");
    cy.contains("1g");
  });
});
