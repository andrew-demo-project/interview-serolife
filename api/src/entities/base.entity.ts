import { ObjectId } from ".pnpm/bson@4.6.1/node_modules/bson";
import { PrimaryKey, Property, SerializedPrimaryKey } from "@mikro-orm/core";

export abstract class BaseEntity {
  @PrimaryKey()
  _id!: ObjectId;

  @SerializedPrimaryKey()
  id!: string;

  @Property()
  createdAd = new Date();

  @Property({ onUpdate: () => new Date() })
  updatedAt = new Date();
}
