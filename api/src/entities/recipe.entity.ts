import { Entity, Filter, Property } from "@mikro-orm/core";
import IngredientDto from "../dtos/Ingredient.dto";
import { BaseEntity } from "./base.entity";

@Entity()
@Filter({
  name: "contains",
  cond: args => ({
    $or: [
      { name: { $regex: args.keyword } },
      { "ingredients.name": { $regex: args.keyword } },
    ],
  }),
})
export default class Recipe extends BaseEntity {
  @Property()
  name: string;

  @Property()
  cookingMethod: string;

  @Property()
  ingredients: IngredientDto[];

  constructor(
    name: string,
    cookingMethod: string,
    ingredients: IngredientDto[]
  ) {
    super();
    this.name = name;
    this.cookingMethod = cookingMethod;
    this.ingredients = ingredients;
  }
}
