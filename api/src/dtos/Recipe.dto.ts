import IngredientDto from "./Ingredient.dto";

export default interface RecipeDto {
  id: number;
  name: string;
  ingredients: IngredientDto[];
  cookingMethod: string;
}
