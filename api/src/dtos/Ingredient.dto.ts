export default interface IngredientDto {
  name: string;
  measurement: string;
}
