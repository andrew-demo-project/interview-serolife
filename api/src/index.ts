import { MikroORM, RequestContext } from "@mikro-orm/core";
import {
  EntityManager,
  EntityRepository,
  MongoDriver,
} from "@mikro-orm/mongodb";
import { json, urlencoded } from "body-parser";
import cors, { CorsOptions } from "cors";
import express, { Request, Response } from "express";
import Recipe from "./entities/recipe.entity";

export const DI = {} as {
  orm: MikroORM<MongoDriver>;
  em: EntityManager;
  recipeRepository: EntityRepository<Recipe>;
};
const app = express();

export class Application {
  constructor() {}

  async setupApplicationSettings() {
    DI.orm = await MikroORM.init<MongoDriver>();
    DI.em = DI.orm.em;
    DI.recipeRepository = DI.orm.em.getRepository(Recipe);

    app.use(cors());
    app.use(urlencoded({ extended: false }));
    app.use(json());
    app.use((req, res, next) => {
      RequestContext.create(DI.orm.em, next);
    });
  }

  async listen() {
    await this.setupApplicationSettings();
    this.setupControllers();

    app.listen(3080, () => console.log("Listening on port 3080"));
  }

  setupControllers() {
    app.get("/recipes", async (req: Request, res: Response) => {
      const keyword = req.query.search ?? "";
      const filter = {
        filters: {
          contains: { keyword },
        },
      };
      const recipes = await DI.recipeRepository.findAll(filter);
      res.status(200).json(recipes);
    });
    app.get("/recipes/:id", async (req: Request, res: Response) => {
      const recipe = await DI.recipeRepository.findOne({
        id: req.params.id,
      });
      res.status(200).json(recipe);
    });
    app.post("/recipes", async (req: Request, res: Response) => {
      if (!req.body.name) {
        res.status(400);
      }
      try {
        const recipe = DI.recipeRepository.create(req.body);
        await DI.recipeRepository.persist(recipe).flush();

        res.json(recipe);
      } catch (e: any) {
        return res.status(400).json({ message: e.message });
      }
    });
    app.delete("/recipes/:id", async (req: Request, res: Response) => {
      try {
        const recipe = await DI.recipeRepository.nativeDelete({
          id: req.params.id,
        });
        res.status(204).end();
      } catch (e: any) {
        return res.status(400).json({ message: e.message });
      }
    });
  }
}

const application = new Application();

application.listen();
