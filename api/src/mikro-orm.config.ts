import { Options } from "@mikro-orm/core";
import { MongoHighlighter } from "@mikro-orm/mongo-highlighter";
import Recipe from "./entities/recipe.entity";

const options: Options = {
  clientUrl: "mongodb://mongo",
  type: "mongo",
  entities: [Recipe],
  dbName: "recipe",
  highlighter: new MongoHighlighter(),
  debug: true,
};

export default options;
