install:
	pnpm install

docker:
	docker compose up

test:
	(cd e2e && pnpm run e2e)